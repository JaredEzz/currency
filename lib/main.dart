import 'package:currency/about.dart';
import 'package:flutter/material.dart';

import 'currency.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(title: 'Flutter Currency Project'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            SizedBox(
            height: 20,
            ),
            Center(
              child:
              Text("Currencies:", style: TextStyle(fontWeight: FontWeight.bold),)
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: map(currencies, (index, c){
                return RaisedButton(
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => CurrencyPage(currency: c,)),);
                  },
                  color: Colors.white,
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Text("${c.name} - ${c.symbol}"),
                  ),
                );
              })
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => AboutPage()),);
        },
        tooltip: 'About',
        child: Icon(Icons.info),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

List currencies = [
  Currency(
    name: "United States Dollar",
    symbol: "\$",
    frontImage: "assets/USD_front.jpg",
    backImage: "assets/USB_back.jpg"
  ),
  Currency(
    name: "Great Britain Pound",
    symbol: "£",
    frontImage: "assets/GBP_front.png",
    backImage: "assets/GBP_back.png"
  ),
  Currency(
    name: "European Union Euro",
    symbol: "€",
    frontImage: "assets/EUR_both.png",
    backImage: "assets/EUR_both.png"
  ),
];

class Currency{
  Currency({this.name, this.symbol, this.imageLink, this.frontImage, this.backImage}){
    this.isFavorite = false;
  }

  String name;
  String symbol;
  String imageLink;
  bool isFavorite;
  String frontImage;
  String backImage;
}

List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }

  return result;
}