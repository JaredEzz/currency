import 'package:currency/main.dart';
import 'package:flutter/material.dart';
import 'dart:core';

class CurrencyPage extends StatefulWidget {
  Currency currency;
  CurrencyPage({this.currency});

  @override
  _CurrencyPageState createState() => _CurrencyPageState(currency: currency);
}

class _CurrencyPageState extends State<CurrencyPage> {
  Currency currency;
  _CurrencyPageState({this.currency});
  Offset _offset = Offset(0.4, 0.7);
  bool _showBack = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("${currency.name}")
          ],
        ),
        actions: <Widget>[
          currency.isFavorite ? IconButton(
            icon: Icon(Icons.star, color: Colors.white,),
            onPressed: (){
              setState(() {
                currency.isFavorite = false;
              });
            },
          ) :
          IconButton(
            icon: Icon(Icons.star_border, color: Colors.white,),
            onPressed: () => setState(() {
              currency.isFavorite = true;
            }),
          )
        ],
      ),
      backgroundColor: Colors.black45,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(30),
            child: Transform(
                transform: Matrix4.identity()
                  ..setEntry(3, 2, 0.001)
                  ..rotateX(0.01 * _offset.dy)
                  ..rotateY(-0.01 * _offset.dx),
                alignment: FractionalOffset.center,
                child: SizedBox(
                    height: MediaQuery.of(context).size.height*0.6,
                    child: GestureDetector(
                      onPanUpdate: (details) {
                        setState((){
                          _offset += details.delta;
                          num offset = (_offset.dx / 150);
                          _showBack = offset.abs() > 1;
                        });
                      },
                      onDoubleTap: (){
                        setState((){
                          _offset = Offset.zero;
                          num offset = (_offset.dx / 150);
                          _showBack = offset.abs() > 1;
                        });
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(5.0),
                        child: !_showBack ? Image.asset(currency.frontImage) : Image.asset(currency.backImage)
                      ),
                    ),
                  ),
                ) 
              )
        ],
      ),
    );
  }
}
