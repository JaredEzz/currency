import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutPage extends StatefulWidget{
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> with TickerProviderStateMixin{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).accentColor,
        title: Text("About Me"),
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      body:
      Container(
        padding: EdgeInsets.only(left: 60.0,right: 60.0,top: 50.0,bottom: 50.0),
        child: Scrollbar(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Text("I'm a student and a developer, or if you're looking to hire, I'm a budding software engineer. I'm experienced in Front End and Back End web development, but I love mobile. Reach out to me with the button at the bottom, I'd love to talk with you. \n \n Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Velit egestas dui id ornare arcu odio ut sem nulla. Et malesuada fames ac turpis egestas sed tempus urna et. Ut tortor pretium viverra suspendisse potenti. Amet aliquam id diam maecenas ultricies mi eget mauris. Vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt. Pellentesque elit eget gravida cum sociis natoque penatibus et. Consectetur lorem donec massa sapien faucibus. Id consectetur purus ut faucibus pulvinar elementum integer enim neque. At quis risus sed vulputate odio ut. Faucibus et molestie ac feugiat sed lectus vestibulum. Nulla aliquet porttitor lacus luctus accumsan tortor posuere ac ut. Diam phasellus vestibulum lorem sed risus ultricies. Etiam tempor orci eu lobortis. Viverra maecenas accumsan lacus vel. Pulvinar etiam non quam lacus suspendisse faucibus interdum posuere. Gravida dictum fusce ut placerat.\n\nPharetra diam sit amet nisl suscipit adipiscing bibendum. Turpis egestas integer eget aliquet nibh praesent tristique magna. Orci eu lobortis elementum nibh tellus molestie nunc. Aliquam ultrices sagittis orci a scelerisque purus. Vitae justo eget magna fermentum iaculis eu non diam phasellus. Neque volutpat ac tincidunt vitae. Ipsum dolor sit amet consectetur adipiscing elit duis tristique. Non sodales neque sodales ut. Id consectetur purus ut faucibus pulvinar. Elit sed vulputate mi sit amet. Integer eget aliquet nibh praesent tristique magna. Ac tortor vitae purus faucibus ornare suspendisse sed. Fringilla ut morbi tincidunt augue interdum velit. Id eu nisl nunc mi ipsum faucibus vitae aliquet nec.\n\nTempor nec feugiat nisl pretium fusce id velit. Pretium nibh ipsum consequat nisl vel pretium lectus quam. Euismod in pellentesque massa placerat duis ultricies lacus sed turpis. Et netus et malesuada fames ac turpis egestas integer eget. Diam donec adipiscing tristique risus. Sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula. At varius vel pharetra vel turpis nunc eget lorem. Condimentum vitae sapien pellentesque habitant morbi tristique. Senectus et netus et malesuada fames ac turpis egestas. Interdum posuere lorem ipsum dolor sit amet.\n\nId ornare arcu odio ut sem nulla pharetra diam sit. Mi proin sed libero enim sed faucibus turpis in eu. Felis imperdiet proin fermentum leo vel orci porta non. Turpis cursus in hac habitasse platea dictumst quisque. Sem viverra aliquet eget sit amet tellus. Ut tortor pretium viverra suspendisse. Sit amet cursus sit amet dictum. Facilisi cras fermentum odio eu feugiat pretium nibh ipsum. Id consectetur purus ut faucibus pulvinar elementum integer. Pellentesque habitant morbi tristique senectus et netus et malesuada.\n\nAc turpis egestas maecenas pharetra convallis posuere. Senectus et netus et malesuada fames. Aliquet porttitor lacus luctus accumsan. Tincidunt tortor aliquam nulla facilisi cras. Viverra suspendisse potenti nullam ac tortor vitae purus. Ornare arcu dui vivamus arcu felis. Imperdiet proin fermentum leo vel orci porta. Pharetra et ultrices neque ornare aenean. Et ultrices neque ornare aenean euismod elementum nisi. In nulla posuere sollicitudin aliquam ultrices sagittis orci.\n\nConvallis posuere morbi leo urna molestie at elementum eu. Turpis in eu mi bibendum neque egestas congue. Faucibus turpis in eu mi. Congue mauris rhoncus aenean vel elit scelerisque mauris. Morbi tristique senectus et netus et malesuada fames. Enim praesent elementum facilisis leo vel. Mus mauris vitae ultricies leo integer malesuada nunc vel risus. Ornare suspendisse sed nisi lacus sed viverra. Arcu felis bibendum ut tristique et egestas quis ipsum suspendisse. Ipsum a arcu cursus vitae congue. Suscipit adipiscing bibendum est ultricies integer quis. Gravida arcu ac tortor dignissim convallis aenean et. Turpis nunc eget lorem dolor sed. Non consectetur a erat nam at lectus urna duis.\n\nEu nisl nunc mi ipsum faucibus vitae aliquet. Mattis enim ut tellus elementum sagittis vitae et leo. Sed euismod nisi porta lorem mollis aliquam ut porttitor leo. Aliquam sem fringilla ut morbi tincidunt augue interdum velit. Consequat mauris nunc congue nisi vitae suscipit tellus mauris. Tempus iaculis urna id volutpat. Arcu risus quis varius quam quisque id diam vel. Mi quis hendrerit dolor magna eget est lorem. Nam at lectus urna duis convallis convallis tellus. Quisque non tellus orci ac auctor augue. Tellus elementum sagittis vitae et leo duis. Nisl purus in mollis nunc sed id semper risus in. Elementum integer enim neque volutpat ac tincidunt vitae. Faucibus et molestie ac feugiat sed lectus vestibulum. Sed lectus vestibulum mattis ullamcorper velit sed ullamcorper morbi. Cursus in hac habitasse platea dictumst. Integer enim neque volutpat ac tincidunt vitae semper quis lectus.\n"),
                  RaisedButton(
                    elevation: 5.0,
                    child: Icon(Icons.email),
                    splashColor: Theme.of(context).accentColor,
                    onPressed: () async {
                      var url = "mailto:jaredezzethasson@gmail.com?";
                      if (await canLaunch(url)) {
                        await launch(url);
                      } else {
                        throw 'Could not launch $url';
                      }
                    },
                  ),
                  SizedBox(
                    height: 20,
                  )
                ],
              ),
            )
        ),
      ),
    );
  }
}

